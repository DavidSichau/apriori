/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#ifndef CSV_H
#define CSV_H

/**
 *@author David Sichau\n
 */

/**
  This class is for handling a csv input file and offers handling of certain rows and columns.
  */

#include <QFile>
#include <QString>
#include <QDebug>
#include <QTextStream>
#include <QStringList>
#include <QMap>

class Csv {
 public:

  Csv(const QString& input_file, QString seperator=";" );

  ~Csv();

  /// prints debug info
  void debug() const;

  /// returns column where the name of the column correspond to the column
  QStringList return_column(const QString& column_name) const;

 private:

  /// generates mapped columns
  void to_mapped_column();

  /// the splited input file
  QVector<QStringList> splited_input_file;

  /// the individual columns
  QMap<QString, QStringList> columns;

};

#endif
