/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#include "rules.h"

void Rules::generate_rules() const{
    if(verbose){
        qDebug()<<"\nGenerating Rules";
    }
    *input_output_manager << "\nAssociation rules:\ncondition ==>";
    *input_output_manager << "consequence (confidence, occurrence)\n";
    QSet<unsigned int> consequence_part;
    rule_assist(rules_trie, consequence_part);
}

/**
  \param trie A pointer to the tree where the rule should be looked for
  \param consequence_part A Set where the items of the consequences are included

*/
void Rules::rule_assist( const Trie* trie, QSet<unsigned int >& consequence_part) const{
    if(consequence_part.size() > 1){
        QSet< unsigned int> condition_part;
        rule_find(condition_part, consequence_part, trie->counter);
    }
    for(QVector<Edge>::const_iterator it_item=trie->edgevector.begin(); it_item!=trie->edgevector.end(); it_item++){
        consequence_part.insert((*it_item).label);
        rule_assist((*it_item).subtrie, consequence_part);
        consequence_part.remove((*it_item).label);
    }
}

/**
  \param condition_part A Set where the items of the condidtion are included
  \param consequence_part A Set where the items of the consequences are included
  \param union_support The counter of the union of the condition and consequence set

*/
void Rules::rule_find(QSet<unsigned int >& condition_part,
                      QSet<unsigned int>& consequence_part, const unsigned int union_support) const{
    unsigned int item;
    for(QSet< unsigned int>::const_iterator it_item=consequence_part.begin(); it_item!=consequence_part.end(); it_item++){
        if(condition_part.empty() || *(--condition_part.end()) < *it_item){
            item=*it_item;
            consequence_part.remove(item);
            condition_part.insert(item);
            if(union_support > rules_trie->is_included(condition_part, condition_part.begin())->counter * min_conf){
                *input_output_manager<< '\n';
                input_output_manager->write_out_basket(condition_part);
                *input_output_manager<< "==> ";
                input_output_manager->write_out_basket(consequence_part);
                *input_output_manager<< "("<<((double) union_support) / rules_trie->is_included(condition_part, condition_part.begin())->counter;
                *input_output_manager<< ", " << ((double)union_support/rules_trie->counter)<< ')';
            }
            else if( consequence_part.size() > 1 ) {
                rule_find(condition_part, consequence_part, union_support);
            }
            if(consequence_part.contains(item)){
                it_item=consequence_part.find(item);
            }
            else{
                it_item = (consequence_part.insert( item )) ;
            }
            condition_part.remove( item );
        }
    }
}
