/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#include "argumentlist.h"
#include <QDebug>

/**
  \param argc The number of elements in *argc[]
  \param argv[] The arguments of the command line
  */
void ArgumentList::argsToStringlist(int argc, char *argv[]){
    for(int i=0; i<argc; i++){
        QString temp=argv[i];
        if(temp[0]!='-'){
            *this+=temp;
        }
        else{
            temp.remove("-");
            options+=temp;
        }
    }
}

void ArgumentList::debug() const{
    qDebug()<<"ArgumentList"<<this->size();
    for(int i=0; i<this->size();i++){
        qDebug()<<at(i);
    }
    qDebug()<<"OptionList"<<options.size();
    for(int i=0; i<options.size();i++){
        qDebug()<<options[i];
    }
}


/**
  \param option Checks if it is in the arguments and removes it from the arguments.
  \return true if the option is in the list
  */
bool ArgumentList::getSwitch(QString option){
    QMutableStringListIterator itr(options);
    while (itr.hasNext()){
        QString temp=itr.next();
        if(temp.contains(option)){
            temp.remove(option);
            itr.remove();
            if(temp.size()>0){
                itr.insert(temp);
            }
            return true;
        }
    }
    return false;
}

/**
  \param option The Argument befor the double
  \param defaultValue The default Value who is returned if the option is not found
  \param lower_bond the return value must be bigger than the lower_bound
  \param upper_bond the return value must be smaller than the upper_bond
  \return a double of element following the Argument
  */
double ArgumentList::getOptionArg(QString option, double defaultValue, double lower_bound, double upper_bond){
    if(options.empty()){
        return defaultValue;
    }
    QMutableStringListIterator itr(options);
    while(itr.hasNext()){
        QString temp=itr.next();
        if(temp.contains(option)){
            temp.remove(option);
            double retval;
            bool ok;
            retval=temp.toDouble(&ok);
            if(ok && lower_bound<=retval && retval<=upper_bond){
                itr.remove();
                return retval;
            }
        }
    }
    return defaultValue;
}
