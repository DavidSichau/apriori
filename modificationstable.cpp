/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#include "modificationstable.h"
#include <limits.h>

static QTextStream cout(stdout, QIODevice::WriteOnly);
static QTextStream cin(stdin, QIODevice::ReadOnly);

/**
  \param input_file_name Name of the input file.
  \param set_verbose If set_verbose = false then no system messages will be written
  during the process.
  */
ModificationsTable::ModificationsTable(const QString& input_file_name,const bool& set_verbose){
    verbose=set_verbose;
    if(verbose){
        qDebug()<<"Open input file";
    }
    csv_input = new Csv(input_file_name);
    if(verbose){
        qDebug()<<"start to read in the transactions";
    }
    parse_input();
    delete_same_column();
    ask_for_columns_to_delete();
    to_hash_table();
}

/**
  \param list a list of strings of integer.
  \return The smallest integer in the list.
  */
int ModificationsTable::minimal_column_element(const QStringList& list)const{
    int temp,min=INT_MAX;
    bool ok;
    for(int i=0; i<list.size(); i++){
        ok =true;
        temp=list.at(i).toInt(&ok);
        if(ok && min>temp){
            min=temp;
        }
    }
    return min;
}

/**
  \param list a list of strings of integer.
  \return The biggest integer in the list.
  */
int ModificationsTable::maximal_column_element(const QStringList& list)const{
    int temp,max=INT_MIN;
    bool ok;
    for(int i=0; i<list.size(); i++){
        ok =true;
        temp=list.at(i).toInt(&ok);
        if(ok && max<temp){
            max=temp;
        }
    }
    return max;
}

/**
  \param  key Key to look for in the hash table
  \return A string corresponding to the given key
  */
QString ModificationsTable::get_value_for_key(const int key)const {
    return hash_table.key(key);
}

void ModificationsTable::parse_input(){
    QString peptide;
    QStringList list_of_peptides=csv_input->return_column("Ambiguous site bracketed");
    QStringList list_of_start_of_peptides=csv_input->return_column("pep_start");
    QStringList list_of_end_of_peptides=csv_input->return_column("pep_end");
    int maximal_peptide_start=maximal_column_element(list_of_start_of_peptides);
    int minimal_peptide_end=minimal_column_element(list_of_end_of_peptides);
    QVector<QString> retval;

    // because numbering in excel file is wrong delete one!
    minimal_peptide_end--;
    maximal_peptide_start--;

    for(int i=0; i<list_of_peptides.size(); i++){
        peptide=list_of_peptides.at(i);
        retval.clear();
        //here also wrong numbering in excel
        if((list_of_start_of_peptides.at(i).toInt()-1)<maximal_peptide_start){
            peptide.remove(0,maximal_peptide_start-(list_of_start_of_peptides.at(i).toInt()-1));
        }
        //here also wrong numbering in excel
        if((list_of_start_of_peptides.at(i).toInt()-1)==1){
            if(peptide.contains("ac-")){
                retval.append("0ac");
                peptide.remove("ac-");
            }
            else{
                retval.append("00");
            }
        }
        QString temp, num;
        for(int j=maximal_peptide_start ;j<minimal_peptide_end; j++){
            temp.clear();
            if(peptide.at(0).isUpper() && peptide[1]=='(' && !peptide.at(2).isUpper() && !peptide.at(3).isUpper()&& peptide[4]==')'){
                //gets modification with (??)
                temp.append(peptide.at(0));
                temp.append(num.setNum(j));
                retval.append(temp);
                peptide.remove(0,5);
            }
            else if(peptide.at(0).isUpper() && peptide[1]=='(' && !peptide.at(2).isUpper() && !peptide.at(3).isUpper()&& !peptide.at(4).isUpper() &&peptide[5]==')'){
                //gets modification with (???)
                temp.append(peptide.at(0));
                temp.append(num.setNum(j));
                retval.append(temp);
                peptide.remove(0,6);
            }
            else if(peptide.at(0).isUpper() && !peptide.at(1).isUpper() && !peptide.at(2).isUpper() && !peptide.at(3).isUpper()&& !peptide.at(4).isUpper()){
                //gets modification with 4 letters
                temp.append(peptide.at(0));
                temp.append(num.setNum(j));
                temp.append(peptide.at(1));
                temp.append(peptide.at(2));
                temp.append(peptide.at(3));
                temp.append(peptide.at(4));
                retval.append(temp);
                peptide.remove(0,5);
            }
            else if(peptide.at(0).isUpper() && !peptide.at(1).isUpper() && !peptide.at(2).isUpper() && !peptide.at(3).isUpper()){
                //gets modifications with 3 letters
                temp.append(peptide.at(0));
                temp.append(num.setNum(j));
                temp.append(peptide.at(1));
                temp.append(peptide.at(2));
                temp.append(peptide.at(3));
                retval.append(temp);
                peptide.remove(0,4);
            }
            else if(peptide.at(0).isUpper() && !peptide.at(1).isUpper() && !peptide.at(2).isUpper()){
                //gets modification with 2 letters
                temp.append(peptide.at(0));
                temp.append(num.setNum(j));
                temp.append(peptide.at(1));
                temp.append(peptide.at(2));
                retval.append(temp);
                peptide.remove(0,3);
            }
            else if(peptide.at(0).isUpper() && !peptide.at(1).isUpper() && peptide.at(1)!='p'){
                //gets modification with 1 letters
                temp.append(peptide.at(0));
                temp.append(num.setNum(j));
                temp.append(peptide.at(1));
                retval.append(temp);
                peptide.remove(0,2);
            }
            else if(peptide.at(1).isUpper() && peptide.at(0)=='p'){
                //gets phosphorylation because its befor
                temp.append(peptide.at(1));
                temp.append(num.setNum(j));
                temp.append(peptide.at(0));
                retval.append(temp);
                peptide.remove(0,2);
            }
            else{
                temp.append(peptide.at(0));
                temp.append(num.setNum(j));
                retval.append(temp);
                peptide.remove(0,1);
            }
        }
        modifications.append(retval);
    }
}

void ModificationsTable::to_hash_table(){
    hash_modifications.resize(modifications.size());
    for(int i=0; i<modifications.size(); i++){
        hash_modifications[i].resize(modifications.at(i).size());
    }
    for(int i=0; i<modifications.size(); i++){
        for(int j=0; j<modifications.at(i).size(); j++){
            if(!hash_table.contains(modifications.at(i).at(j))){
                hash_table.insert(modifications.at(i).at(j),hash_table.size());
                hash_modifications[i][j]=hash_table.value(modifications.at(i).at(j));
            }
            else{
                hash_modifications[i][j]=hash_table.value(modifications.at(i).at(j));
            }
        }
    }
}

void ModificationsTable::ask_for_columns_to_delete() {
    print_Modification_table(true);
    cout<<"Please insert the number of the rows who should get deleted, seperate each number in a new line type -1 when finished"<<endl;
    int i=-2;
    QList<int> columns_to_delete;
    while(i!=-1){
        if(i>-2 && i<modifications.at(i).size() ){
            columns_to_delete.append(i);
        }
        cin>>i;
    }
    if(verbose){
        qDebug()<<"You have deleted"<<columns_to_delete.size()<<"columns";
    }
    delete_columns(columns_to_delete);
}


/**
  \param columns_to_delete A List of columns to delete
  */
void ModificationsTable::delete_columns(QList<int>& columns_to_delete){
    qSort(columns_to_delete);
    for(int i=0; i<modifications.size();i++){
        for(int j=columns_to_delete.size()-1; j>=0; j--){
            modifications[i].remove(columns_to_delete[j]);
        }
    }
}

void ModificationsTable::delete_same_column(){
    QList<int> equal_column;
    for(int j=0; j<modifications.at(0).size();j++){
        if(are_all_column_elements_equal(j)){
            equal_column.append(j);
        }
    }
    if(verbose){
        qDebug()<<"There were"<<equal_column.size()<< "columns deleted, where all amino acids modifications where the same";
    }
    delete_columns(equal_column);
}

/**
  \param column_nb Number of column where all entries should be checked
  \return true if all columns are equal
  */
bool ModificationsTable::are_all_column_elements_equal(const int column_nb)const {
    QString prev_row=modifications.at(0).at(column_nb);
    for(int i=1; i<modifications.size(); i++){
        if(prev_row!=modifications.at(i).at(column_nb)){
            return false;
        }
        prev_row=modifications.at(i).at(column_nb);
    }
    return true;
}


/**
  \param outfile Name of the file where the output should be written to
  */
void ModificationsTable::print_Modification_table_to_file(const QString& outfile){
    QFile output_file(outfile);
    if (!output_file.open(QIODevice::WriteOnly| QIODevice::Text)){
        qFatal("error in writing the output file");
    }
    QTextStream out_stream( &output_file);
    for(int i=0;i<modifications.size();i++){
        for(int j=0; j<modifications.at(i).size();j++){
            out_stream<<modifications.at(i).at(j)<<"\t";
        }
        out_stream<<endl;
    }
    output_file.close();
}

/**
  \param outfile Name of the file where the output should be written to
  */
void ModificationsTable::print_hash_modifications_to_file(const QString& outfile){
    QFile output_file(outfile);
    if (!output_file.open(QIODevice::WriteOnly| QIODevice::Text)){
        qFatal("error in writing the output file");
    }
    QTextStream out_stream(&output_file);
    for(int i=0; i< hash_modifications.size(); i++ ){
        for(int j=0; j<hash_modifications.at(i).size(); j++){
            out_stream<<hash_modifications.at(i).at(j)<<"\t";
        }
        out_stream<<endl;
    }
    output_file.close();
}

/**
  \param label_rows True if the number of the rows should be printed also (default false)
  */
void ModificationsTable::print_Modification_table(const bool label_rows) const{
    for(int i=0;i<modifications.size();i++){
        for(int j=0; j<modifications.at(i).size();j++){
            cout<<modifications.at(i).at(j)<<"\t";
        }
        cout<<endl;
    }
    if(label_rows){
        for(int i=0; i<modifications.at(1).size();i++){
            cout<<i<<"\t";
        }
    }
    cout<<endl;
}

void ModificationsTable::print_hash_modifications() const{
    for(int i=0;i<hash_modifications.size();i++){
        for(int j=0; j<hash_modifications.at(i).size();j++){
            cout<<hash_modifications.at(i).at(j)<<"\t";
        }
        cout<<endl;
    }
    cout<<endl;
}

ModificationsTable::~ModificationsTable(){
    delete csv_input;
}
