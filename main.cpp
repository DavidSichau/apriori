/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */

#include <QDebug>
#include <ctime>
#include "Constants.h"
#include "argumentlist.h"
#include "modificationstable.h"
#include "Apriori/Apriori.hpp"
#include "rules.h"
#include "Csv.h"

static QTextStream cout(stdout, QIODevice::WriteOnly);
static QTextStream cin(stdin, QIODevice::ReadOnly);
static QTextStream cerr(stderr, QIODevice::WriteOnly);

void usage(QString appname){
    cout<<"USAGE: "<<appname<<" [-options] infile outfile\n";
    cout<<"type: "<<appname<<" -! for help\n";
}

static void help (void)
{	cout << "OPTIONS:\n";
    cout << "\"-!\"\t for help\n";
    cout << "\"-s#\"\t to define the minimal support for the patterns. The value should be between 0 and 1 \t (default: "<<DEFAULT_SUPPORT<<") \n";
    cout << "\"-c#\"\t to define the minimal confidence of the rules. The value should be between 0 and 1 \t (default:"<< DEFAULT_CONFIDENCE<<")\n";
    cout << "\"-q\" \t to suppress progress informations of the program.\n";
    cout << "\"-t#\" \t to define the size threshold for the frequent patterns. 0 if treshold should be infinite.\n\t The value must be an integer between 0 and 100 \t (default:"<<DEFAULT_ITEMSIZE<<")\n";
    cout << endl;

    exit(0);
}

int main(int argc, char *argv[])
{
    ArgumentList al(argc, argv);
    QString appname= al.takeFirst();
    if(al.getSwitch("!")){
        cout << "\nHELP: \n";
        usage(appname);
        help();
    }
    if(al.count()<2){
        usage(appname);
        exit(1);
    }
    // verbose mode: if it is false, than no system messages will be written out during the program.
    bool verbose = true;
    verbose = !al.getSwitch("q");

    double min_supp=al.getOptionArg("s", DEFAULT_SUPPORT);
    double min_conf=al.getOptionArg("c", DEFAULT_CONFIDENCE);
    unsigned int size_threshold=(int)al.getOptionArg("t",DEFAULT_ITEMSIZE,0,100);

    QString input_file =al.takeFirst();
    QString output_file=al.takeFirst();

    //begin apriori

    clock_t start;
    start = clock();
    ModificationsTable tables_of_modifications(input_file,verbose);
    if(verbose){
        cout<<"Runtime for the genertion of the Modification table: "<< ( clock() - start ) / (double)CLOCKS_PER_SEC <<"s"<<endl;
    }
    start= clock();
    Apriori apriori(output_file, tables_of_modifications);
    *apriori.get_input_manager()<<"Parameters used for this output:";
    *apriori.get_input_manager()<<"\nMinimal support: \t\t"<<min_supp;
    *apriori.get_input_manager()<<"\nMinimal Confidence: \t"<<min_conf;
    *apriori.get_input_manager()<<"\nSize treshold: \t\t\t"<<size_threshold<<"\n\n";
    apriori.APRIORI_alg( min_supp, verbose, size_threshold );
    if(verbose){
        cout<<"Runtime of Apriori: "<< ( clock() - start ) / (double)CLOCKS_PER_SEC <<"s"<<endl;
    }
    start= clock();
    Rules rules(apriori.get_input_manager(),min_conf, verbose);
    rules.generate_rules();
    if(verbose){
        cout<<"Runtime for generating the rules: "<< ( clock() - start ) / (double)CLOCKS_PER_SEC <<"s"<<endl;
    }
}

