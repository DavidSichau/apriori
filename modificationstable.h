/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#ifndef MODIFICATIONSTABLE_H
#define MODIFICATIONSTABLE_H

/**
 *@author David Sichau\n
 */

/**
  This class is for handling the the histone peptides with the post translational modifications.
  The peptides are stored in rows and the amino acids with modifications are stored in the columns.
  In the end the amino acids with modifications are hashed to integer for the apriori algorithm.

*/


#include <QString>
#include <QVector>
#include <QFile>
#include <QTextStream>
#include <QMap>
#include <QtAlgorithms>
#include <QDebug>
#include <QStringList>
#include "Csv.h"

class ModificationsTable
{
    friend class Input_Output_Manager;

 public:
    ModificationsTable(const QString& input_file_name,const bool& set_verbose);

    /// returns the value to the integer key form elements stored in the hash table
    QString get_value_for_key(const int key) const;

    ~ModificationsTable();

 private:
    /// Matix of histone peptides with modifications
    QVector< QVector<QString> > modifications;

    /// Matrix of hashed histone peptides with modifications
    QVector< QVector<int> > hash_modifications;

    /// map of the corresponding hash keys
    QMap< QString, int> hash_table;

    /// parse line to process the input
    void parse_input();

    /// checks if all element of one column are equal
    bool are_all_column_elements_equal(const int column_nb) const;

    /// delete column if all elemenst are equal
    void delete_same_column();

    /// delete columns who are in the list.
    void delete_columns(QList<int>& columns_to_delete);

    /// ask the user which columns should get deleted and delete them
    void ask_for_columns_to_delete();

    /// finds the minimal element in a column
    int minimal_column_element(const QStringList& list) const;

    /// finds the maximum element in a column
    int maximal_column_element(const QStringList& list) const;

    /// converts modification table to the hash modifcations and fills the hash table
    void to_hash_table();

    /// true if system messages will be written during the process
    bool verbose;

    /// print table of modifications to command line
    void print_Modification_table(const bool label_rows=false) const;

    /// prints table of modifications to file
    void print_Modification_table_to_file(const QString& outfile);

    /// print hashed modifications to the command line
    void print_hash_modifications() const;

    /// print table of hashed modifications to file
    void print_hash_modifications_to_file(const QString& outfile);

    /// pointer to the csv file for the input
    Csv* csv_input;

};

#endif // MODIFICATIONSTABLE_H
