#-------------------------------------------------
#
# Project created by QtCreator 2010-10-14T11:00:56
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = rules_program
CONFIG += console
CONFIG -= app_bundle
CONFIG += static

TEMPLATE = app


SOURCES += main.cpp \
    argumentlist.cpp \
    modificationstable.cpp \
    Apriori/Trie.cpp \
    Apriori/Apriori.cpp \
    Apriori/Apriori_Trie.cpp \
    rules.cpp \
    Apriori/Input_Output_Manager.cpp \
    Csv.cpp

HEADERS += \
    argumentlist.h \
    modificationstable.h \
    Apriori/Trie.hpp \
    Apriori/Apriori.hpp \
    Apriori/Apriori_Trie.hpp \
    rules.h \
    Apriori/Input_Output_Manager.h \
    Csv.h \
    Constants.h
