/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#include "Csv.h"

/**
  \param input_file_name name of the input file
  \param seperator the sperator of the csv file (default ";")
  */
Csv::Csv(const QString& input_file_name, QString seperator ){
    QFile input_file(input_file_name);
    if (!input_file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qFatal("error in reading the input file");
    }
    QTextStream in(&input_file);
    QString line;
    while (!in.atEnd()) {
        line = in.readLine();
        splited_input_file.append(line.split(seperator));
    }
    input_file.close();
    to_mapped_column();
}

/**
  \param column_name a string corresponding to the name of a column
  \return A column as a list of strings
  */
QStringList Csv::return_column(const QString& column_name) const{
    return columns.value(column_name);
}

Csv::~Csv(){
}

void Csv::to_mapped_column(){
    QVector<QString> keys;
    for(int j=0; j<splited_input_file.at(0).size(); j++){
        keys.append(splited_input_file.at(0).at(j));
    }
    for(int j=0; j<splited_input_file.at(0).size(); j++){
        QStringList temp;
        temp.clear();
        for(int i=1; i<splited_input_file.size(); i++){
            temp.append(splited_input_file.at(i).at(j));
        }
        columns.insert(keys.at(j),temp);
    }
}

void Csv::debug() const{
    for(int i=0; i<splited_input_file.size(); i++){
        for(int j=0; j<splited_input_file.at(i).size(); j++){
            qDebug()<<splited_input_file.at(i).at(j);
        }
    }
}
