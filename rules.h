/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#ifndef RULES_H
#define RULES_H

/**
  @author David Sichau\n
  */

#include "Apriori/Input_Output_Manager.h"
#include "Apriori/Trie.hpp"
#include <QSet>

/**
  This class generates rules out of a Trie with frequent itemssets.

*/

class Rules
{
 public:
  /**
    \param input_output_manager a pointer to the input output manager
    \param min_conf The minimal confident of a rule.
    \param set_verbose If set_verbose = false then no system messages will be written
    during the process.
    */
  Rules(Input_Output_Manager* input_output_manager, double min_conf, bool verbose):
      input_output_manager(input_output_manager),rules_trie(input_output_manager->get_output_trie()),
      verbose(verbose),min_conf(min_conf){}

  /// Generates the rules for the rules trie
  void generate_rules() const;

  /// Assists in generating rules
  void rule_assist( const Trie* trie, QSet<unsigned int >& consequence_part) const;

  /// Finds the rules
  void rule_find(QSet<unsigned int >& condition_part,
                 QSet<unsigned int>& consequence_part, const unsigned int union_support) const;

 private:
  /// Pointer to the input output manager
  Input_Output_Manager* input_output_manager;

  /// Pointer to the tree with all frequent items
  Trie* rules_trie;
  /// true if system messages will be written during the process
  bool verbose;

  /// The minimal confident of the rules
  double min_conf;
};

#endif // RULES_H
