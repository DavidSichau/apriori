/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#ifndef ARGUMENTLIST_H
#define ARGUMENTLIST_H

/**
 *@author David Sichau\n
 */

/**
  This class is for handling the command line arguments
  */

#include <QStringList>

class ArgumentList : public QStringList
{
 public:

  ArgumentList(int argc, char * argv[]){
      argsToStringlist(argc, argv);
  }

  /// checks of Argument is given in options
  bool getSwitch(QString option);

  /// returns the next Argument after the option as double (e.g. -c0.4)
  double getOptionArg(QString option, double defaultRetValue, double lower_bound=0, double upper_bond=1);

  /// prints debug info
  void debug() const;

 private:
  /// parses the arguments
  void argsToStringlist(int argc, char *argv[]);

  /// list of options
  QStringList options;
};

#endif // ARGUMENTLIST_H
