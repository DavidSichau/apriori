/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#ifndef Trie_HPP
#define Trie_HPP

/**
 *@author Ferenc Bodon
 *@author David Sichau\n
 */

#include <QVector>
#include <QSet>

class Apriori_Trie;
class Trie;

/** This struct represent an edge of a Trie.

  An edge has a label, and an edge points to a subtrie.
  */
struct Edge
{
    unsigned int label;
    Trie* subtrie;
    /**
      \param edge The Edge the current is compared with.
      \return True if the labels of the Eges are the same.
      */
    bool operator==(const Edge &edge) const{return label == edge.label;}
};

/** This class represent a general Trie.

  We can regard the trie as a recursive data structure. It has a root
  node and a list of (sub)trie. We can reach a subtree by a
  labeled edge (link). Since the root of the trie represents an itemset
  the counter stands for the occurrence. For the sake of fast traversal
  we also store the length of the maximal path starting from the root,
  and the edges are stored ordered according to their label.
  */

class Trie
{
    friend class Apriori_Trie;
    friend class Rules;

 public:
    /**
      \param init_counter The initial counter of the new trie
      */
    Trie( const unsigned int init_counter ):counter(init_counter){}

    /// It decides whether the given itemset is included in the trie or not.
    const Trie* is_included( const QSet<unsigned int>& an_itemset,
                            QSet<unsigned int>::const_iterator item_it ) const;

    /// Increases the counter for those itemsets that is
    /// contained by the given basket.
    void find_candidate( QVector<unsigned int>::const_iterator it_basket_upper_bound,
                        QVector<unsigned int>::const_iterator it_basket,
                        const unsigned int counter_incr );

    /// Deletes the tries that represent infrequent itemsets.
    void delete_infrequent( const double min_occurrence );

    /// Inserts a itemset in a trie
    void insert_itemset(QSet<unsigned int> itemset, const unsigned int counter);

    /// Prints the tree in a format for graphviz
    void show_trie_content(const int parent_node) const;

    /// Prints the tree in a format for graphviz
    void show_trie_content() const;

    ~Trie();

 private:

    /// Adds an empty state to the trie
    void add_empty_state( const unsigned int item, const unsigned int init_counter=0 );

    /// counter stores the occurrence of the itemset represented by the Trie
    unsigned int counter;

    /** edgevector stores the edges of the root the trie.
     *
     * edgevector is always sorted!
     */
    QVector<Edge> edgevector;
};
#endif
