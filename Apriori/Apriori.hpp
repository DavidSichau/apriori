/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#ifndef APRIORI_H
#define APRIORI_H

#include "Apriori_Trie.hpp"
#include <QMap>
#include <QString>
#include <QSet>
#include <QVector>


/**
 *@author Bodon Ferenc
 *@author David Sichau \n
 */

/** This class implements the APRIORI algirithm.

  <p>
  APRIORI is a levelwise algorithm.
  It scans the transaction database several times.
  After the first scan the frequent 1-itemsets are found, and in general
  after the <em>k<sup>th</sup></em> scan the frequent <em>k</em>-itemsets
  are extracted. The method does not determine the support of every possible
  itemset. In an attempt to narrow the domain to be searched, before every pass
  it generates <em>candidate</em> itemsets. An itemset becomes a candidate
  if every subset of it is frequent. Obviously every frequent itemset
  needs to be candidate too, hence only the support of candidates is calculated.
  Frequent <em>k</em>-itemsets generate the candidate <em>k+1</em>-itemsets
  after the \f$k^{th}\f$ scan.
  </p>

  <p>
  After all the candidate <em>k+1</em>-itemsets have been generated, a new
  scan of the transactions is effected and the precise support of the
  candidates is determined. The candidates with low support are thrown away. The
  algorithm ends when no candidates can be generated.
  </p>

  <p>
  The intuition behind candidate generation is based on the following simple
fact:<br><div align="center"><em>Every subset of a frequent itemset
is frequent.</em></div><br> This is immediate, because if a transaction
<em>t</em> supports an itemset <em>X</em>, then <em>t</em> supports
every subset \f$Y\subseteq X\f$.
</p>

<p>
Using the fact indirectly, we infer, that if an itemset has a subset that is
infrequent, then it cannot be frequent. So in the algorithm APRIORI only those
itemsets will be candidates whose every subset is frequent. The frequent
<em>k</em>-itemsets are available when we attempt to generate candidate
<em>k+1</em>-itemsets. The algorithm seeks candidate <em>k+1</em>-itemsets
among the sets which are unions of two frequent <em>k</em>-itemsets. After
forming the union we need to verify that all of its subsets are frequent,
otherwise it should not be a candidate. To this end, it is clearly enough to
check if all the <em>k</em>-subsets of <em>X</em> are frequent.
</p>

<p>
Next the supports of the candidates are calculated. This is done by reading
transactions one by one. For each transaction <em>t</em> the algorithm decides
which candidates are supported by <em>t</em>. To solve this task efficiently
APRIORI uses a hash-tree. However in this implementation a trie (prefix-tree)
is applied. Tries have many advantages over hash-trees.
<ol>
<li> It is faster </li>
<li> It needs no parameters (main drawback of a hash-tree is that its
performance is very sensitive to the parameteres) </li>
<li> The candidate generation is very simple. </li>
</ol>
</p>
*/

class Apriori {
 public:

  /**
    \param mod_table The Database that contain the transactions.
    \param output_file_name The name of file where the results have to be
    written to.
    */
  Apriori(  QString output_file_name, ModificationsTable& mod_table):
      input_output_manager( output_file_name, mod_table ){}

  /// This procedure implements the APRIORI algorithm
  void APRIORI_alg( const double min_supp, const bool verbose, const unsigned int size_threshold );

  Input_Output_Manager* get_input_manager();

  ~Apriori(){delete apriori_trie;}
 private:

  /// Determines the support of the candidates of the given size
  void support( const unsigned int& candidate_size );

  /// A trie that stores the candidates.
  Apriori_Trie*                           apriori_trie;

  /// The input_output_manager that is responsibel for the input,
  /// output and recoding operations.
  Input_Output_Manager                    input_output_manager;
};

#endif
