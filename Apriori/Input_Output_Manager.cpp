/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#include "Input_Output_Manager.h"

/**
  \param output_file_name Name of the output file
  \param mod_table The Database where the transactions are saved
  */
Input_Output_Manager::Input_Output_Manager(const QString& output_file_name ,ModificationsTable& mod_table):
    mod_table(&mod_table){
        nb_of_baskets=mod_table.hash_modifications.size();
        output_trie= new Trie(nb_of_baskets);
        out_file=new QFile (output_file_name);
        if (!out_file->open(QIODevice::WriteOnly | QIODevice::Text)){
            qFatal("Error in opening the output file");
        }
        this->setDevice(out_file);
    }

/**
  \param basket The basket that will be filled with the next row of the file.
  \param row_number Row who will be read in
  \return 0 if the end of the transactions is reached, otherwise 1;
  */
int Input_Output_Manager::read_in_a_line( QSet<unsigned int>& basket, int row_number ){
    if( row_number >= mod_table->hash_modifications.size()  ) {
        return 0;
    }
    basket.clear();
    for(int j=0; j< mod_table->hash_modifications.at(row_number).size() ;j++){
        basket.insert( (unsigned int)mod_table->hash_modifications.at(row_number).at(j));
    }
    return 1;
}

/**
  \param min_supp The (relative) support threshold.
  \param support_of_items The support of the items.
  The i<sup>th</sup> least frequent item's support is
  given by support_of_items[i].
  \return The number of transactions that the transaction database contains.
  */
unsigned int Input_Output_Manager::find_frequent_items(const double min_supp, QVector<unsigned int>& support_of_items )
{
    unsigned int basket_number = 0;
    QSet<unsigned int> basket;
    QVector< unsigned int > temp_counter_vector;

    /// Determining the support of the items
    QSet<unsigned int>::iterator it_basket;
    while( read_in_a_line( basket, basket_number ) ){
        if( !basket.empty() ){
            basket_number++;
            for( it_basket = basket.begin(); it_basket != basket.end(); it_basket++ ){
                if( (int)(*it_basket + 1)  > temp_counter_vector.size() )
                    temp_counter_vector.resize( *it_basket + 1 );
                temp_counter_vector[*it_basket]++;
            }
        }
    }

    /// Finding the frequent items
    double min_occurrence = min_supp * (basket_number - 0.5);
    QVector<unsigned int>::size_type edgeIndex;

    QSet< QPair<unsigned int, unsigned int> > temp_set;
    for( edgeIndex = 0; edgeIndex < temp_counter_vector.size(); edgeIndex++ )
        if( temp_counter_vector[edgeIndex] > min_occurrence )
            temp_set.insert(QPair<unsigned int, unsigned int>(temp_counter_vector[edgeIndex], edgeIndex));

    new_code_inverse.clear();
    support_of_items.clear();
    for(QSet< QPair<unsigned int, unsigned int> >::iterator it = temp_set.begin(); it != temp_set.end(); it++){
        new_code_inverse.push_back((*it).second);
        support_of_items.push_back((*it).first);
    }
    new_code_inverse.squeeze();
    support_of_items.squeeze();

    new_code.reserve(  temp_counter_vector.size() + 1 );
    new_code.resize(  temp_counter_vector.size() + 1 );

    for( edgeIndex = 0; edgeIndex < new_code_inverse.size(); edgeIndex++ )
        new_code[new_code_inverse[edgeIndex]] = edgeIndex+1;

    nb_of_baskets=basket_number;
    return basket_number;
}

/**
  \param original_basket The basket to filter and recode.
  \param new_basket The created reduced basket
  */
void Input_Output_Manager::basket_recode(const QSet<unsigned int>& original_basket, QVector<unsigned int>& new_basket )
{
    new_basket.clear();
    for( QSet<unsigned int>::const_iterator it_basket = original_basket.begin();
        it_basket != original_basket.end(); it_basket++ )
        if( new_code[*it_basket] ) new_basket.push_back( new_code[*it_basket]-1 );
    qSort( new_basket.begin(), new_basket.end() );
}

/**
  \param basket Basket written to output file
  */
void Input_Output_Manager::write_out_basket(const QSet<unsigned int>& basket){
    for( QSet<unsigned int>::const_iterator it_item = basket.begin();
        it_item != basket.end(); it_item++)
    {
        operator<<(mod_table->get_value_for_key((int)new_code_inverse[*it_item]))<<(' ');
    }
}

/**
  \param basket Itemset written to output file
  \param counter Number of occurences ot the Itemset in the Transaction Database
  */
void Input_Output_Manager::write_out_basket_and_counter(
    const QSet<unsigned int>& itemset, const unsigned int counter)
{
    output_trie->insert_itemset( itemset,  counter);

    for( QSet<unsigned int>::const_iterator it_item = itemset.begin();
        it_item != itemset.end(); it_item++)
    {
        operator <<(mod_table->get_value_for_key((int)new_code_inverse[*it_item]))<<(' ');
    }

    operator <<('(')<<((double)counter/nb_of_baskets)<<(")\n");

    output_trie->insert_itemset(itemset,counter);
}

/**
  \return A Pointer to the output_trie
  */
Trie* Input_Output_Manager::get_output_trie() const{
    return output_trie;
}

Input_Output_Manager::~Input_Output_Manager(){
    delete output_trie;
    out_file->close();
    delete out_file;
}
