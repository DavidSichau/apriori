/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#include "Apriori.hpp"
#include <QDebug>

/**
  \param candidate_size The size of the candidate whose support
  has top be determined.
  */
void Apriori::support( const unsigned int& candidate_size )
{
    QSet<unsigned int> basket;
    QVector<unsigned int> basket_v;

    int line_number=0;
    while( input_output_manager.read_in_a_line( basket , line_number ) ){
        line_number++;
        input_output_manager.basket_recode(basket, basket_v);
        apriori_trie->find_candidate(basket_v,candidate_size);
    }
}

/**
  \param min_supp The relative support threshold
  \param verbose If verbose = false then no system messages will be written
  during the process.
  \param size_threshold Frequent itemsets above this threshold
  do not need to be found.
  */
void Apriori::APRIORI_alg( const double min_supp, const bool verbose, const unsigned int size_threshold )
{
    unsigned int basket_number;

    if(verbose) qDebug()<<endl<<"Finding frequent itemsets..."<<endl;
    unsigned int candidate_size=1;
    if(verbose)
    {
        qDebug()<<"Determining the support of the items and deleting infrequent ones!";
    }
    QVector<unsigned int> support_of_items;

    basket_number = input_output_manager.find_frequent_items(min_supp, support_of_items );
    input_output_manager<< "Frequent 0-itemsets:\nitemset (occurrence)\n";
    input_output_manager<< "{} ("<< basket_number << ")\n";
    input_output_manager<< "Frequent " << 1;
    input_output_manager<< "-itemsets:\nitemset (occurrence)\n";
    QSet<unsigned int> temp_set;
    for(QVector<unsigned int>::size_type index = 0;index < support_of_items.size(); index++)
    {
        temp_set.insert(index);

        input_output_manager.write_out_basket_and_counter(temp_set, support_of_items[index]);
        temp_set.erase(temp_set.begin());
    }

    apriori_trie = new Apriori_Trie( basket_number );
    apriori_trie->insert_frequent_items( support_of_items );

    double min_supp_abs = min_supp * (basket_number - 0.5);
    candidate_size++;
    if(verbose)
    {
        qDebug()<<endl<<"Genarating "<<candidate_size<<"-itemset candidates!";
    }
    apriori_trie->candidate_generation(candidate_size-1,input_output_manager);

    while( apriori_trie->is_there_any_candidate() )
    {
        if(verbose)
        {
            qDebug()<<"Determining the support of the "<<candidate_size<<"-itemset candidates!";
        }
        support( candidate_size );
        if(verbose) qDebug()<<"Deleting infrequent itemsets!";
        apriori_trie->delete_infrequent(min_supp_abs, candidate_size);

        if (candidate_size == size_threshold )
        {
            if(verbose) qDebug()<<"Size threshold is reached!";
            break;
        }
        candidate_size++;
        if( verbose )
        {
            qDebug()<<endl<<"Genarating "<<candidate_size<<"-itemset candidates!";
        }
        input_output_manager<< "Frequent " << candidate_size-1;
        input_output_manager<< "-itemsets:\nitemset (occurrence)\n";
        apriori_trie->candidate_generation(candidate_size-1, input_output_manager);
    }
    if(verbose) qDebug()<<endl<<"All Frequent items are recorded!";
}

Input_Output_Manager* Apriori::get_input_manager(){
    return &input_output_manager;
}
