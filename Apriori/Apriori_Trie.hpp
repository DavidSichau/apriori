/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#ifndef Apriori_Trie_HPP
#define Apriori_Trie_HPP

#include <QSet>
#include <QVector>
#include "Trie.hpp"
#include "Input_Output_Manager.h"
/**
  @author Ferenc Bodon
  @author David Sichau\n
  */

/**
  Apriori_Trie (or prefix-tree) is a tree-based datastructure.

  Apriori_Trie is a special tree designed to provide efficient methods
  for the apriori algorithm. It mostly uses a regular trie except
  when there exist faster solution. For example for storing one and two
  itemset candidate where a simple vector and array gives better performance.
  Apriori_Trie extends the functions provided by the regular trie with
  a candidate generation process.

*/
class Apriori_Trie
{
 public:

  /**
    \param counter_of_emptyset The support of the empty set,
    i.e. the number of transactions.
    */
  Apriori_Trie( const unsigned int counter_of_emptyset ):
      main_trie(counter_of_emptyset){}

  /// Prints the Apriori Tree as input for graphviz to the command line
  void show_content();

  /// Insert the frequent items and their counters into the trie;
  void insert_frequent_items(const QVector<unsigned int>& counters );

  /** Generates candidates
    Generates candidates and writes frequent itemset
    that are obtained in the previous iteration to disk.
    */
  void candidate_generation( const unsigned int& frequent_size, Input_Output_Manager& input_output_manager );



  /// Increases the counter of those candidates that are contained
  /// by the given basket.
  void find_candidate( const QVector<unsigned int>& basket,
                      const unsigned int candidate_size,
                      const unsigned int counter=1 );

  /// Deletes unfrequent itemsets.
  void delete_infrequent( const double min_occurrence,const unsigned int candidate_size);

  /// Returns true if trie is not empty
  bool is_there_any_candidate() const {
      return !main_trie.edgevector.empty();
  }

  ~Apriori_Trie(){}

 private:

  /// Decides if all subset of an itemset is contained in the Apriori_Trie
  bool is_all_subset_frequent( const QSet<unsigned int>& maybe_candidate ) const;

  /// Generates candidate of size two
  void candidate_generation_two();

  /// Generates candidate of size more than two
  void candidate_generation_assist(
      Trie* trie,
      QSet<unsigned int>& maybe_candidate,
      Input_Output_Manager& input_output_manager);

  /// Increases the counter for those itempairs that are in the given basket.
  void find_candidate_two( const QVector<unsigned int>& basket,
                          const unsigned int counter=1 );

  /// Deletes the Tries that represent infrequent itemsets of size 2.
  void delete_infrequent_two( const double min_occurrence );

  /// Trie to store the candidates.
  Trie main_trie;

  /**  temp_counter_array stores the occurences of the itempairs
   *
   * We can use a simple array to determine the support of itemset
   * of size two. This requires less memory than the trie-based supportcount.
   * temp_counter_array[i][j-i] stores the occurence of the itempair (i,j).
   */
  QVector< QVector<unsigned int> > temp_counter_array;
};

#endif
