/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#include "Trie.hpp"
#include <QTextStream>

static QTextStream cout(stdout, QIODevice::WriteOnly);

bool Edge_point_less(const Edge edge, const unsigned int label){
    return edge.label < label;
}

/**
  \param an_itemset The given itemset.
  \param item_it This iterator shows the element of the basket
  that has to be processed.
  \return NULL, if the itemset is not included, otherwise the trie,
  that represents the itemset.
  */
const Trie* Trie::is_included( const QSet<unsigned int>& an_itemset,
                              QSet<unsigned int>::const_iterator item_it ) const
{
    if( item_it == an_itemset.end() ) return this;
    else {
        QVector<Edge>::const_iterator it_edgevector =
            qLowerBound (edgevector.begin(), edgevector.end(),
                         *item_it, Edge_point_less);
        if( it_edgevector != edgevector.end() && (*it_edgevector).label == *item_it )
            return (*it_edgevector).subtrie->is_included( an_itemset, ++item_it );
        else return NULL;
    }
}

/**
  \param basket the given basket
  \param it_basket *it_basket lead to the actual Trie.
  Only items following this item in the basket
  need to be considered
  \param counter_incr The number times this basket occurs
  */
void Trie::find_candidate(
    QVector<unsigned int>::const_iterator it_basket_upper_bound,
    QVector<unsigned int>::const_iterator it_basket,
    const unsigned int counter_incr )
{
    if( edgevector.empty() )
        counter += counter_incr;
    else{
        QVector<Edge>::iterator it_edge = edgevector.begin();
        while( it_edge != edgevector.end() && it_basket != it_basket_upper_bound ){
            if( (*it_edge).label < *it_basket) ++it_edge;
            else if( (*it_edge).label > *it_basket) ++it_basket;
            else {
                (*it_edge).subtrie->find_candidate( it_basket_upper_bound + 1,it_basket + 1,
                                                   counter_incr);
                ++it_edge;
                ++it_basket;
            }
        }
    }
}

/**
  \param min_occurrence The occurence threshold
  */
void Trie::delete_infrequent( const double min_occurrence ){
    QVector<Edge>::iterator itEdge = edgevector.begin();
    while(itEdge != edgevector.end() ){
        if( (*itEdge).subtrie->edgevector.empty() ){
            if( (*itEdge).subtrie->counter < min_occurrence ){
                delete (*itEdge).subtrie;
                itEdge = edgevector.erase(itEdge);
            }
            else ++itEdge;
        }
        else{
            (*itEdge).subtrie->delete_infrequent( min_occurrence );
            if( (*itEdge).subtrie->edgevector.empty() ) {
                delete (*itEdge).subtrie;
                itEdge = edgevector.erase(itEdge);
            }
            else ++itEdge;
        }
    }
}

void Trie::show_trie_content() const{
    cout<<"digraph G\n{\n node [shape = record];\n";
    cout<<"node0 ;"<<endl;
    show_trie_content(0);
    cout<<"}"<<endl;
}

/**
  \param parent_node The label of the parent_node
  */
void Trie::show_trie_content(int const parent_node) const{
    cout<<"node"<<parent_node<<"[label=\"counter: "<<counter<<"\"];"<<endl;

    for(int i=0; i<edgevector.size();i++){
        int new_node=parent_node*10;
        new_node=new_node+i+1;
        cout<<"\"node"<<parent_node<<"\"->\"node"<<new_node<<"\"[label=\""<<edgevector.at(i).label<<"\"];\n";
        if(edgevector.at(i).subtrie->edgevector.size()>0){
            edgevector.at(i).subtrie->show_trie_content(new_node);
        }
        else{
            cout<<"node"<<new_node<<"[label=\" counter: "<<edgevector.at(i).subtrie->counter<<"\"];"<<endl;
        }
    }
}

/**
  \param itemset Itemset to insert in Trie
  \param counter The counter of the given itemset
  */
void Trie::insert_itemset(QSet<unsigned int> itemset, const unsigned int counter){
    if(itemset.size()==1){
        Edge temp_edge;
        temp_edge.label = *itemset.begin();
        temp_edge.subtrie = new Trie( counter );
        if(!edgevector.contains(temp_edge)){
            edgevector.push_back(temp_edge);
        }
        else{
            delete temp_edge.subtrie;
        }
    }
    else{
        for( QVector<Edge>::iterator itEdge = edgevector.begin(); itEdge != edgevector.end(); ++itEdge ){
            if((*itEdge).label==*itemset.begin()){
                itemset.erase(itemset.begin());
                (*itEdge).subtrie->insert_itemset(itemset,counter);
            }
        }
    }
}

Trie::~Trie(){
    for( QVector<Edge>::iterator itEdge = edgevector.begin(); itEdge != edgevector.end(); ++itEdge )
        delete (*itEdge).subtrie;
}

/**
  \param item The label of the new edge
  \param counter The initial counter of the new state
  */
void Trie::add_empty_state( const unsigned int item, const unsigned int counter ){
    Edge temp_edge;
    temp_edge.label = item;
    temp_edge.subtrie = new Trie( counter );
    edgevector.push_back(temp_edge);
}
