/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#ifndef Input_Output_Manager_HPP
#define Input_Output_Manager_HPP

/**
 *@author David Sichau\n
 */

#include <QVector>
#include <QString>
#include <QSet>
#include <QPair>
#include "modificationstable.h"
#include "Trie.hpp"

/**  This class is responsible for the input, output and recode operations.

  In frequent itemset mining (FIM) algorithms only frequent items
  are of interest. Hence it is useful to represent frequent items with
integers: <em>1, 2, ..., n</em>, where <em>n</em> is the number of
frequent items.

*/

class Input_Output_Manager: public QTextStream
{
 public:
  Input_Output_Manager(const QString& output_file_name , ModificationsTable& mod_table);

  /// Reads in one transaction from the basketfile.
  int read_in_a_line( QSet<unsigned int>& basket, int row_number );

  /// Determines the frequent items,
  /// fills in the new_code an new_code_inverse vectors
  unsigned int find_frequent_items(const double min_supp, QVector<unsigned int>& support_of_items );

  /// Creates an other basket that contains only the frequent items recoded.
  void basket_recode( const QSet<unsigned int>& original_basket,
                     QVector<unsigned int>& new_basket );

  /// Writes out an itemset to the output file
  void write_out_basket( const QSet<unsigned int>& itemset ) ;

  /// Writes out an itemset and its counter to the output file.
  /// Writes the frequent itemsets to a trie
  void write_out_basket_and_counter( const QSet<unsigned int>& itemset,
                                    const unsigned int counter);
  /// Returns pointer to the output trie
  Trie* get_output_trie() const;

  /// Pointer to the output file
  QFile* out_file;

  ~Input_Output_Manager( );

 private:

  /// A Modification table that stores the original Database
  ModificationsTable* mod_table;

  /// A Trie where all frequent items are included
  Trie* output_trie;

  /** The new codes of the frequent items.
   *
   * if new_code[i] is 0, then i is not frequent, otherwise the
   * new code of item i is new_code[i]-1.
   */
  QVector<unsigned int> new_code;

  /** The inverse of new_code vector.
   *
   * new_code_inverse[new_code[i]-1]=i if i is a frequent item.
   */
  QVector<unsigned int> new_code_inverse;

  /// the number of all baskets (transactions
  int nb_of_baskets;
};

#endif
