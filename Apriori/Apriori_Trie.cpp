/* Copyright (c) 2012 David Sichau <mail"at"sichau"dot"eu>

   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
   LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
   SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
   */
#include "Apriori_Trie.hpp"

/**
  \param counters It stores the support of the items.
  counters[i] stores the suport of item i.
  */
void Apriori_Trie::insert_frequent_items(const QVector<unsigned int>& counters )
{
    for(QVector<unsigned int>::size_type item_index = 0; item_index < counters.size(); item_index++)
        main_trie.add_empty_state( item_index, counters[item_index] );
}

/**
  \param frequent_size Size of the frequent itemsets that
  generate the candidates.
  */
void Apriori_Trie::candidate_generation(const unsigned int& frequent_size, Input_Output_Manager& input_output_manager)
{
    if( frequent_size == 1 ){
        candidate_generation_two();
    }
    else
    {
        QSet<unsigned int> maybe_candidate;
        candidate_generation_assist( &main_trie, maybe_candidate,input_output_manager);
    }
}

/**
  \param basket The basket that hs to be analyzed.
  \param candidate_size the size of the candidates.
  \param counter_incr The number of time the basket occured.
  The counters of candidates that occure in the basket has
  to be incremented by counter_incr.
  */
void Apriori_Trie::find_candidate( const QVector<unsigned int>& basket,
                                  const unsigned int candidate_size,
                                  const unsigned int counter_incr)
{
    if( candidate_size != 2 )
        if ( (int)candidate_size<basket.size()+1 )
            main_trie.find_candidate( basket.end()-candidate_size+1,
                                     basket.begin(), counter_incr );
        else{;}
    else find_candidate_two( basket, counter_incr );
}

/**
  \param min_occurrence The threshold of absolute support.
  \param candidate_size The size of the candidate itemset.
  */
void Apriori_Trie::delete_infrequent( const double min_occurrence,
                                     const unsigned int candidate_size )
{
    if( candidate_size != 2 )
        main_trie.delete_infrequent( min_occurrence );
    else delete_infrequent_two( min_occurrence );
}

/**
  \param maybe_candidate The itemset that has to be checked.
  */
bool Apriori_Trie::is_all_subset_frequent(const QSet<unsigned int>& maybe_candidate ) const
{
    if( maybe_candidate.size() < 3) return true; // because of the						// candidate generation method!
    else
    {
        QSet<unsigned int> temp_itemset(maybe_candidate);
        QSet<unsigned int>::const_iterator item_it = --(--maybe_candidate.end());
        do
        {
            item_it--;
            temp_itemset.remove( *item_it );
            if( !main_trie.is_included( temp_itemset, temp_itemset.begin() ) )
                return false;
            temp_itemset.insert( *item_it );
        }
        while ( item_it != maybe_candidate.begin() );
        return true;
    }
}

void Apriori_Trie::candidate_generation_two()
{
    if( !main_trie.edgevector.empty() ){
        temp_counter_array.reserve(main_trie.edgevector.size()-1);
        temp_counter_array.resize(main_trie.edgevector.size()-1);
        for( QVector<Edge>::size_type stateIndex = 0; stateIndex < main_trie.edgevector.size()-1; stateIndex++ ){
            temp_counter_array[stateIndex].reserve(main_trie.edgevector.size()-1-stateIndex );
            temp_counter_array[stateIndex].resize( main_trie.edgevector.size()-1-stateIndex);
        }
    }
}

void Apriori_Trie::candidate_generation_assist(
    Trie* trie,
    QSet<unsigned int>& maybe_candidate,
    Input_Output_Manager& input_output_manager)
{
    QVector<Edge>::iterator itEdge = trie->edgevector.begin();
    if( (*itEdge).subtrie->edgevector.empty() ){
        QVector<Edge>::iterator itEdge2;
        Trie* toExtend;
        while( itEdge != trie->edgevector.end() ){
            maybe_candidate.insert((*itEdge).label);
            toExtend = (*itEdge).subtrie;
            input_output_manager.write_out_basket_and_counter(maybe_candidate, toExtend->counter);
            for( itEdge2 = itEdge + 1; itEdge2 != trie->edgevector.end();itEdge2++ ){
                maybe_candidate.insert( (*itEdge2).label );
                if( is_all_subset_frequent( maybe_candidate) )
                    toExtend->add_empty_state( (*itEdge2).label );
                maybe_candidate.remove( (*itEdge2).label );
            }
            (QVector<Edge>(toExtend->edgevector)).squeeze ();

            maybe_candidate.remove((*itEdge).label);
            if( toExtend->edgevector.empty() ){
                delete (*itEdge).subtrie;
                itEdge = trie->edgevector.erase(itEdge);
            }
            else itEdge++;
        }
    }
    else{
        while( itEdge != trie->edgevector.end() ){
            maybe_candidate.insert((*itEdge).label);
            candidate_generation_assist((*itEdge).subtrie,
                                        maybe_candidate, input_output_manager );
            maybe_candidate.remove((*itEdge).label);
            if((*itEdge).subtrie->edgevector.empty()){
                delete (*itEdge).subtrie;
                itEdge = trie->edgevector.erase(itEdge);
            }
            else itEdge++;
        }
    }
}

void Apriori_Trie::show_content(){
    main_trie.show_trie_content();
}

/**
  \param basket the given basket
  \param counter The number the processed basket occures
  in the transactional database
  */
void Apriori_Trie::find_candidate_two( const QVector<unsigned int>& basket,
                                      const unsigned int counter )
{
    if( basket.size() > 1){
        QVector<unsigned int>::const_iterator it1_basket,  it2_basket;
        for( it1_basket = basket.begin(); it1_basket != basket.end()-1; it1_basket++)
            for( it2_basket = it1_basket+1; it2_basket != basket.end();it2_basket++)
                temp_counter_array[*it1_basket][*it2_basket-*it1_basket-1]+= counter;
    }
}

/**
  \param min_occurrence The occurence threshold
  */
void Apriori_Trie::delete_infrequent_two( const double min_occurrence )
{
    QVector<Edge>::size_type stateIndex_1, stateIndex_2;
    for( stateIndex_1 = 0; stateIndex_1 < main_trie.edgevector.size()-1; stateIndex_1++ ){
        for( stateIndex_2 = 0;
            stateIndex_2 < main_trie.edgevector.size() - 1 - stateIndex_1;
            stateIndex_2++ )
        {
            if( temp_counter_array[stateIndex_1][stateIndex_2] > min_occurrence )
                main_trie.edgevector[stateIndex_1].subtrie->add_empty_state(
                    stateIndex_1 + stateIndex_2 + 1,
                    temp_counter_array[stateIndex_1][stateIndex_2] );
        }
        temp_counter_array[stateIndex_1].clear();
    }
    temp_counter_array.clear();
    QVector<Edge>::iterator it= main_trie.edgevector.begin();
    while( it!=main_trie.edgevector.end() )
        if((*it).subtrie->edgevector.empty()){
            delete (*it).subtrie;
            it = main_trie.edgevector.erase(it);
        }
        else it++;
}
